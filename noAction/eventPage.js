// https://developer.chrome.com/extensions/background_pages
/*
  Background scripts are called when:
  - The extension is first installed or updated to a new version
  - The background page was listening for an event, and the event is dispatched
  - A content script or other extension sends a message
  - Another view in the extension, such as a popup, calls the runtime.getBackgroundPage
*/

var menuItem = {
  "id": "wikit",
  "title": "Wikit",
  // Where do we wayynt this menu item to appear
  "contexts": ["selection"]
}

chrome.contextMenus.create(menuItem);

function fixedEncodeURI (str) {
  return encodeURI(str).replace(/%5B/g, '[').replace(/%5D/g, ']');
}

chrome.contextMenus.onClicked.addListener(function(clickData) {
  if (clickData.menuItemId === "wikit" && clickData.selectionText) {
    var wikiUrl = "https://en.wikipedia.org/wiki/" + fixedEncodeURI(clickData.selectionText);
    var createData = {
      "url": wikiUrl,
      "type": "popup",
      "top": 5,
      "left": 5,
      "width": screen.availWidth/2,
      "height": screen.availHeight/2
    };
    chrome.windows.create(createData, function() {});
  }
});