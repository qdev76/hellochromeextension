Debugging

Popup
- Click "Inspect Popup" in the menu of the extension icon

Options
- Go to the options page
- F12

Background Pages
- Go to Extensions page in Chrome
- Click on the background page under your extension to load it
- F12 on the popup

Content Scripts
- Go to page
- F12
- Go to Sources
- Go to Content Scripts tab on the left section

Deploying to Chrome Web Store

https://chrome.google.com/webstore/developer/dashboard

- Uploaded as a draft
- Don't forget to click Publish