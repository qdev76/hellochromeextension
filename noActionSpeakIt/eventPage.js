// https://developer.chrome.com/extensions/background_pages
/*
  Background scripts are called when:
  - The extension is first installed or updated to a new version
  - The background page was listening for an event, and the event is dispatched
  - A content script or other extension sends a message
  - Another view in the extension, such as a popup, calls the runtime.getBackgroundPage
*/
chrome.contextMenus.removeAll();
var menuItem = {
  "id": "speak",
  "title": "Speak",
  // Where do we want this menu item to appear
  "contexts": ["selection"]
}
chrome.contextMenus.create(menuItem);

chrome.contextMenus.onClicked.addListener(function(clickData) {
  if (clickData.menuItemId === "speak" && clickData.selectionText) {
    chrome.tts.speak(clickData.selectionText, {'rate': 0.7});
  }
});