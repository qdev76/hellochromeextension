// https://developer.chrome.com/extensions/background_pages
/*
  Background scripts are called when:
  - The extension is first installed or updated to a new version
  - The background page was listening for an event, and the event is dispatched
  - A content script or other extension sends a message
  - Another view in the extension, such as a popup, calls the runtime.getBackgroundPage
*/
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  if (request.todo = "showPageAction") {
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
      chrome.pageAction.show(tabs[0].id);
    });
  }
});