// https://developer.chrome.com/extensions/background_pages
/*
  Background scripts are called when:
  - The extension is first installed or updated to a new version
  - The background page was listening for an event, and the event is dispatched
  - A content script or other extension sends a message
  - Another view in the extension, such as a popup, calls the runtime.getBackgroundPage
*/

var contextMenuItem = {
  "id": "spendMoney",
  "title": "SpendMoney",
  // Where do we want this menu item to appear
  "contexts": ["selection"]
};
chrome.contextMenus.create(contextMenuItem);
chrome.contextMenus.onClicked.addListener(function(clickData) {
  if (clickData.menuItemId == "spendMoney" && clickData.selectionText) {
    if (isInt(clickData.selectionText)) {
      chrome.storage.sync.get(['total','limit'], function(budget) {
        var newTotal = 0;
        if (budget.total) {
          newTotal += parseInt(budget.total);
        }
        newTotal += parseInt(clickData.selectionText);
        chrome.storage.sync.set({'total': newTotal}, function() {
          if (newTotal >= budget.limit) {
            var notifOptions = {
              type: 'basic',
              iconUrl: 'icon48.png',
              title: 'Limit reached!',
              message: "Uh oh! Looks like you've reached your limit!"
            };

            chrome.notifications.create('limitNotif', notifOptions);
          }
        });
      });
    }
  }
});

function isInt(value) {
  return  !isNaN(value) &&
          parseInt(Number(value)) == value &&
          !isNaN(parseInt(value, 10));
}

chrome.storage.onChanged.addListener(function(changes, storageName) {
  chrome.browserAction.setBadgeText({"text": changes.total.newValue.toString()});
});